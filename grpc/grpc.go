package grpc

import (
	"gitlab.com/final_exam2/organization_service/config"
	"gitlab.com/final_exam2/organization_service/genproto/organization_service"
	"gitlab.com/final_exam2/organization_service/grpc/client"
	"gitlab.com/final_exam2/organization_service/grpc/service"
	"gitlab.com/final_exam2/organization_service/pkg/logger"
	"gitlab.com/final_exam2/organization_service/storage"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {
	grpcServer = grpc.NewServer()

	organization_service.RegisterBranchServiceServer(grpcServer, service.NewBranchService(cfg, log, strg, srvc))
	organization_service.RegisterProviderServiceServer(grpcServer, service.NewProviderService(cfg, log, strg, srvc))
	organization_service.RegisterMarketServiceServer(grpcServer, service.NewMarketService(cfg, log, strg, srvc))
	organization_service.RegisterStaffServiceServer(grpcServer, service.NewStaffService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
