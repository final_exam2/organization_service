package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.com/final_exam2/organization_service/config"
	"gitlab.com/final_exam2/organization_service/genproto/organization_service"
	"gitlab.com/final_exam2/organization_service/grpc/client"
	"gitlab.com/final_exam2/organization_service/pkg/logger"
	"gitlab.com/final_exam2/organization_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type ProviderService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*organization_service.UnimplementedProviderServiceServer
}

func NewProviderService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *ProviderService {
	return &ProviderService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *ProviderService) Create(ctx context.Context, req *organization_service.ProviderCreate) (*organization_service.Provider, error) {
	u.log.Info("====== Provider Create ======", logger.Any("req", req))

	resp, err := u.strg.Provider().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create Provider: u.strg.Provider().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ProviderService) GetById(ctx context.Context, req *organization_service.ProviderPrimaryKey) (*organization_service.Provider, error) {
	u.log.Info("====== Provider Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.Provider().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While Provider Get By ID: u.strg.Provider().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ProviderService) GetList(ctx context.Context, req *organization_service.ProviderGetListRequest) (*organization_service.ProviderGetListResponse, error) {
	u.log.Info("====== Provider Get List ======", logger.Any("req", req))

	resp, err := u.strg.Provider().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While Provider Get List: u.strg.Provider().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ProviderService) Update(ctx context.Context, req *organization_service.ProviderUpdate) (*organization_service.Provider, error) {
	u.log.Info("====== Provider Update ======", logger.Any("req", req))

	resp, err := u.strg.Provider().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While Provider Update: u.strg.Provider().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ProviderService) Delete(ctx context.Context, req *organization_service.ProviderPrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== Provider Delete ======", logger.Any("req", req))

	err := u.strg.Provider().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While Provider Delete: u.strg.Provider().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
