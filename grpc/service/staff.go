package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.com/final_exam2/organization_service/config"
	"gitlab.com/final_exam2/organization_service/genproto/organization_service"
	"gitlab.com/final_exam2/organization_service/grpc/client"
	"gitlab.com/final_exam2/organization_service/pkg/logger"
	"gitlab.com/final_exam2/organization_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type StaffService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*organization_service.UnimplementedStaffServiceServer
}

func NewStaffService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *StaffService {
	return &StaffService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *StaffService) Create(ctx context.Context, req *organization_service.StaffCreate) (*organization_service.Staff, error) {
	u.log.Info("====== Staff Create ======", logger.Any("req", req))

	resp, err := u.strg.Staff().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create Staff: u.strg.Staff().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *StaffService) GetById(ctx context.Context, req *organization_service.StaffPrimaryKey) (*organization_service.Staff, error) {
	u.log.Info("====== Staff Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.Staff().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While Staff Get By ID: u.strg.Staff().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *StaffService) GetList(ctx context.Context, req *organization_service.StaffGetListRequest) (*organization_service.StaffGetListResponse, error) {
	u.log.Info("====== Staff Get List ======", logger.Any("req", req))

	resp, err := u.strg.Staff().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While Staff Get List: u.strg.Staff().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *StaffService) Update(ctx context.Context, req *organization_service.StaffUpdate) (*organization_service.Staff, error) {
	u.log.Info("====== Staff Update ======", logger.Any("req", req))

	resp, err := u.strg.Staff().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While Staff Update: u.strg.Staff().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *StaffService) Delete(ctx context.Context, req *organization_service.StaffPrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== Provider Delete ======", logger.Any("req", req))

	err := u.strg.Staff().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While Staff Delete: u.strg.Staff().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
