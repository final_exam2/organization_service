package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.com/final_exam2/organization_service/config"
	"gitlab.com/final_exam2/organization_service/genproto/organization_service"
	"gitlab.com/final_exam2/organization_service/grpc/client"
	"gitlab.com/final_exam2/organization_service/pkg/logger"
	"gitlab.com/final_exam2/organization_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type MarketService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*organization_service.UnimplementedMarketServiceServer
}

func NewMarketService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *MarketService {
	return &MarketService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *MarketService) Create(ctx context.Context, req *organization_service.MarketCreate) (*organization_service.Market, error) {
	u.log.Info("====== Market Create ======", logger.Any("req", req))

	resp, err := u.strg.Market().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create Market: u.strg.Market().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *MarketService) GetById(ctx context.Context, req *organization_service.MarketPrimaryKey) (*organization_service.Market, error) {
	u.log.Info("====== Market Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.Market().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While Market Get By ID: u.strg.Market().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *MarketService) GetList(ctx context.Context, req *organization_service.MarketGetListRequest) (*organization_service.MarketGetListResponse, error) {
	u.log.Info("====== Market Get List ======", logger.Any("req", req))

	resp, err := u.strg.Market().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While Market Get List: u.strg.Market().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *MarketService) Update(ctx context.Context, req *organization_service.MarketUpdate) (*organization_service.Market, error) {
	u.log.Info("====== Market Update ======", logger.Any("req", req))

	resp, err := u.strg.Market().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While Market Update: u.strg.Market().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *MarketService) Delete(ctx context.Context, req *organization_service.MarketPrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== Market Delete ======", logger.Any("req", req))

	err := u.strg.Market().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While Market Delete: u.strg.Market().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
