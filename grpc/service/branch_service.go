package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.com/final_exam2/organization_service/config"
	"gitlab.com/final_exam2/organization_service/genproto/organization_service"
	"gitlab.com/final_exam2/organization_service/grpc/client"
	"gitlab.com/final_exam2/organization_service/pkg/logger"
	"gitlab.com/final_exam2/organization_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type BranchService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*organization_service.UnimplementedBranchServiceServer
}

func NewBranchService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *BranchService {
	return &BranchService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *BranchService) Create(ctx context.Context, req *organization_service.BranchCreate) (*organization_service.Branch, error) {
	u.log.Info("====== Branch Create ======", logger.Any("req", req))

	resp, err := u.strg.Branch().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create Branch: u.strg.Branch().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *BranchService) GetById(ctx context.Context, req *organization_service.BranchPrimaryKey) (*organization_service.Branch, error) {
	u.log.Info("====== Branch Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.Branch().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While Branch Get By ID: u.strg.Branch().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *BranchService) GetList(ctx context.Context, req *organization_service.BranchGetListRequest) (*organization_service.BranchGetListResponse, error) {
	u.log.Info("====== Branch Get List ======", logger.Any("req", req))

	resp, err := u.strg.Branch().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While Branch Get List: u.strg.Branch().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *BranchService) Update(ctx context.Context, req *organization_service.BranchUpdate) (*organization_service.Branch, error) {
	u.log.Info("====== Branch Update ======", logger.Any("req", req))

	resp, err := u.strg.Branch().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While Branch Update: u.strg.Branch().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *BranchService) Delete(ctx context.Context, req *organization_service.BranchPrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== Branch Delete ======", logger.Any("req", req))

	err := u.strg.Branch().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While Branch Delete: u.strg.Branch().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
