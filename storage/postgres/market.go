package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/final_exam2/organization_service/genproto/organization_service"
	"gitlab.com/final_exam2/organization_service/pkg/helper"
)

type MarketRepo struct {
	db *pgxpool.Pool
}

func NewMarketRepo(db *pgxpool.Pool) *MarketRepo {
	return &MarketRepo{
		db: db,
	}
}

func (r *MarketRepo) Create(ctx context.Context, req *organization_service.MarketCreate) (*organization_service.Market, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO market(id, name, branch_id, updated_at)
		VALUES ($1, $2, $3,NOW())
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.Name,
		req.BranchId,
	)

	if err != nil {
		return nil, err
	}

	return &organization_service.Market{
		Id:       id,
		Name:     req.Name,
		BranchId: req.BranchId,
	}, nil
}

func (r *MarketRepo) GetByID(ctx context.Context, req *organization_service.MarketPrimaryKey) (*organization_service.Market, error) {

	var (
		query string

		id         sql.NullString
		name       sql.NullString
		branch_id  sql.NullString
		created_at sql.NullString
		updated_at sql.NullString
	)

	query = `
		SELECT
			id,
			name,
			branch_id,
			created_at,
			updated_at		
		FROM market
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&branch_id,
		&created_at,
		&updated_at,
	)

	if err != nil {
		return nil, err
	}

	return &organization_service.Market{
		Id:        id.String,
		Name:      name.String,
		BranchId:  branch_id.String,
		CreatedAt: created_at.String,
		UpdatedAt: updated_at.String,
	}, nil
}

func (r *MarketRepo) GetList(ctx context.Context, req *organization_service.MarketGetListRequest) (*organization_service.MarketGetListResponse, error) {

	var (
		resp   = &organization_service.MarketGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			branch_id,
			created_at,
			updated_at		
		FROM market
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Search != "" {
		where += ` AND name ILIKE '%' || '` + req.Search + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id         sql.NullString
			name       sql.NullString
			branch_id  sql.NullString
			created_at sql.NullString
			updated_at sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&branch_id,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.Markets = append(resp.Markets, &organization_service.Market{
			Id:        id.String,
			Name:      name.String,
			BranchId:  branch_id.String,
			CreatedAt: created_at.String,
			UpdatedAt: updated_at.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *MarketRepo) Update(ctx context.Context, req *organization_service.MarketUpdate) (*organization_service.Market, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
		market
		SET
			name = :name,
			branch_id = :branch_id,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":        req.GetId(),
		"name":      req.GetName(),
		"branch_id": req.GetBranchId(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &organization_service.Market{
		Id:       req.Id,
		Name:     req.Name,
		BranchId: req.BranchId,
	}, nil
}

func (r *MarketRepo) Delete(ctx context.Context, req *organization_service.MarketPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM market WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
