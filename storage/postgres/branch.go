package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/final_exam2/organization_service/genproto/organization_service"
	"gitlab.com/final_exam2/organization_service/pkg/helper"
)

type BranchRepo struct {
	db *pgxpool.Pool
}

func NewBranchRepo(db *pgxpool.Pool) *BranchRepo {
	return &BranchRepo{
		db: db,
	}
}

func (r *BranchRepo) Create(ctx context.Context, req *organization_service.BranchCreate) (*organization_service.Branch, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO branch(id, name, branch_code, address, phone_number, updated_at)
		VALUES ($1, $2, $3, $4, $5 , NOW())
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.Name,
		req.BranchCode,
		req.Address,
		req.PhoneNumber,
	)

	if err != nil {
		return nil, err
	}

	return &organization_service.Branch{
		Id:          id,
		Name:        req.Name,
		BranchCode:  req.BranchCode,
		Address:     req.Address,
		PhoneNumber: req.PhoneNumber,
	}, nil
}

func (r *BranchRepo) GetByID(ctx context.Context, req *organization_service.BranchPrimaryKey) (*organization_service.Branch, error) {

	var (
		query string

		id           sql.NullString
		name         sql.NullString
		branch_code  sql.NullString
		address      sql.NullString
		phone_number sql.NullString
		created_at   sql.NullString
		updated_at   sql.NullString
	)

	query = `
		SELECT
			id,
			name,
			branch_code,
			address,
			phone_number,
			created_at,
			updated_at		
		FROM branch
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&branch_code,
		&address,
		&phone_number,
		&created_at,
		&updated_at,
	)

	if err != nil {
		return nil, err
	}

	return &organization_service.Branch{
		Id:          id.String,
		Name:        name.String,
		BranchCode:  branch_code.String,
		Address:     address.String,
		PhoneNumber: phone_number.String,
		CreatedAt:   created_at.String,
		UpdatedAt:   updated_at.String,
	}, nil
}

func (r *BranchRepo) GetList(ctx context.Context, req *organization_service.BranchGetListRequest) (*organization_service.BranchGetListResponse, error) {

	var (
		resp   = &organization_service.BranchGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			branch_code,
			address,
			phone_number,
			created_at,
			updated_at		
		FROM branch
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Search != "" {
		where += ` AND name ILIKE '%' || '` + req.Search + `' || '%' AND phone_number ILIKE '%' || '` + req.Search + `' || '%' `
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id           sql.NullString
			name         sql.NullString
			branch_code  sql.NullString
			address      sql.NullString
			phone_number sql.NullString
			created_at   sql.NullString
			updated_at   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&branch_code,
			&address,
			&phone_number,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.Branches = append(resp.Branches, &organization_service.Branch{
			Id:          id.String,
			Name:        name.String,
			BranchCode:  branch_code.String,
			Address:     address.String,
			PhoneNumber: phone_number.String,
			CreatedAt:   created_at.String,
			UpdatedAt:   updated_at.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *BranchRepo) Update(ctx context.Context, req *organization_service.BranchUpdate) (*organization_service.Branch, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			branch
		SET
			name = :name,
			branch_code = :branch_code,
			address = :address,
			phone_number = :phone_number,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":           req.GetId(),
		"name":         req.GetName(),
		"branch_code":  req.GetBranchCode(),
		"address":      req.GetAddress(),
		"phone_number": req.GetPhoneNumber(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &organization_service.Branch{
		Id:          req.Id,
		Name:        req.Name,
		BranchCode:  req.BranchCode,
		Address:     req.Address,
		PhoneNumber: req.PhoneNumber,
	}, nil
}

func (r *BranchRepo) Delete(ctx context.Context, req *organization_service.BranchPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM branch WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
