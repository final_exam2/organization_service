package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/final_exam2/organization_service/genproto/organization_service"
	"gitlab.com/final_exam2/organization_service/pkg/helper"
	"golang.org/x/crypto/bcrypt"
)

type StaffRepo struct {
	db *pgxpool.Pool
}

func NewStaffRepo(db *pgxpool.Pool) *StaffRepo {
	return &StaffRepo{
		db: db,
	}
}

func (r *StaffRepo) Create(ctx context.Context, req *organization_service.StaffCreate) (*organization_service.Staff, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO staff(id, name,surname, phone_number, login, password,user_type, market_id ,updated_at)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, NOW())
	`
	bytes, err := bcrypt.GenerateFromPassword([]byte(req.Password), 14)
	if err != nil {
		return nil, err
	}
	_, err = r.db.Exec(ctx, query,
		id,
		req.Name,
		req.Surname,
		req.PhoneNumber,
		req.Login,
		string(bytes),
		req.UserType,
		req.MarketId,
	)

	if err != nil {
		return nil, err
	}

	return &organization_service.Staff{
		Id:          id,
		Name:        req.Name,
		Surname:     req.Surname,
		PhoneNumber: req.PhoneNumber,
		Login:       req.Login,
		Password:    req.Password,
		UserType:    req.UserType,
		MarketId:    req.MarketId,
	}, nil
}

func (r *StaffRepo) GetByID(ctx context.Context, req *organization_service.StaffPrimaryKey) (resp *organization_service.Staff, err error) {

	var (
		query string

		id           sql.NullString
		name         sql.NullString
		surname      sql.NullString
		phone_number sql.NullString
		login        sql.NullString
		password     sql.NullString
		user_type    sql.NullString
		market_id    sql.NullString
		created_at   sql.NullString
		updated_at   sql.NullString
	)

	var where = "id"

	if len(req.Login) > 0 {
		where = " login "
		req.Id = req.Login
	}

	query = `
		SELECT
			id,
			name,
			surname,
			phone_number,
			login,
			password,
			user_type,
			market_id,
			created_at,
			updated_at		
		FROM staff
		WHERE  ` + where + ` = $1
	`

	err = r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&surname,
		&phone_number,
		&login,
		&password,
		&user_type,
		&market_id,
		&created_at,
		&updated_at,
	)

	if err != nil {
		return nil, err
	}

	return &organization_service.Staff{
		Id:          id.String,
		Name:        name.String,
		Surname:     surname.String,
		PhoneNumber: phone_number.String,
		Login:       login.String,
		Password:    password.String,
		UserType:    user_type.String,
		MarketId:    market_id.String,
		CreatedAt:   created_at.String,
		UpdatedAt:   updated_at.String,
	}, nil
}

func (r *StaffRepo) GetList(ctx context.Context, req *organization_service.StaffGetListRequest) (*organization_service.StaffGetListResponse, error) {

	var (
		resp   = &organization_service.StaffGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			surname,
			phone_number,
			login,
			password,
			user_type,
			market_id,
			created_at,
			updated_at		
		FROM staff
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Search != "" {
		where += ` AND name ILIKE '%' || '` + req.Search + `' || '%'  AND surname ILIKE '%' || '` + req.Search + `' || '%'  AND phone_number ILIKE '%' || '` + req.Search + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id           sql.NullString
			name         sql.NullString
			surname      sql.NullString
			phone_number sql.NullString
			login        sql.NullString
			password     sql.NullString
			user_type    sql.NullString
			market_id    sql.NullString
			created_at   sql.NullString
			updated_at   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&surname,
			&phone_number,
			&login,
			&password,
			&user_type,
			&market_id,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.Staffs = append(resp.Staffs, &organization_service.Staff{
			Id:          id.String,
			Name:        name.String,
			Surname:     surname.String,
			PhoneNumber: phone_number.String,
			Login:       login.String,
			Password:    password.String,
			UserType:    user_type.String,
			MarketId:    market_id.String,
			CreatedAt:   created_at.String,
			UpdatedAt:   updated_at.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *StaffRepo) Update(ctx context.Context, req *organization_service.StaffUpdate) (*organization_service.Staff, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
		staff
		SET
			name = :name,
			surname = :surname,
			phone_number = :phone_number,
			login =: login,
			password =: password,
			user_type =: user_type,
			market_id =: market_id,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":           req.GetId(),
		"name":         req.GetName(),
		"surname":      req.GetSurname(),
		"phone_number": req.GetPhoneNumber(),
		"login":        req.GetLogin(),
		"password":     req.GetPassword(),
		"user_type":    req.GetUserType(),
		"market_id":    req.GetMarketId(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &organization_service.Staff{
		Id:          req.Id,
		Name:        req.Name,
		Surname:     req.Surname,
		PhoneNumber: req.PhoneNumber,
		Login:       req.Login,
		Password:    req.Password,
		UserType:    req.UserType,
		MarketId:    req.MarketId,
	}, nil
}

func (r *StaffRepo) Delete(ctx context.Context, req *organization_service.StaffPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM staff WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
