package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/final_exam2/organization_service/genproto/organization_service"
	"gitlab.com/final_exam2/organization_service/pkg/helper"
)

type ProviderRepo struct {
	db *pgxpool.Pool
}

func NewProviderRepo(db *pgxpool.Pool) *ProviderRepo {
	return &ProviderRepo{
		db: db,
	}
}

func (r *ProviderRepo) Create(ctx context.Context, req *organization_service.ProviderCreate) (*organization_service.Provider, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO provider(id, name, phone_number, active, updated_at)
		VALUES ($1, $2, $3, $4, NOW())
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.Name,
		req.PhoneNumber,
		req.Active,
	)

	if err != nil {
		return nil, err
	}

	return &organization_service.Provider{
		Id:          id,
		Name:        req.Name,
		PhoneNumber: req.PhoneNumber,
		Active:      req.Active,
	}, nil
}

func (r *ProviderRepo) GetByID(ctx context.Context, req *organization_service.ProviderPrimaryKey) (*organization_service.Provider, error) {

	var (
		query string

		id           sql.NullString
		name         sql.NullString
		phone_number sql.NullString
		active       sql.NullString
		created_at   sql.NullString
		updated_at   sql.NullString
	)

	query = `
		SELECT
			id,
			name,
			phone_number,
			active,
			created_at,
			updated_at		
		FROM provider
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&phone_number,
		&active,
		&created_at,
		&updated_at,
	)

	if err != nil {
		return nil, err
	}

	return &organization_service.Provider{
		Id:          id.String,
		Name:        name.String,
		PhoneNumber: phone_number.String,
		Active:      active.String,
		CreatedAt:   created_at.String,
		UpdatedAt:   updated_at.String,
	}, nil
}

func (r *ProviderRepo) GetList(ctx context.Context, req *organization_service.ProviderGetListRequest) (*organization_service.ProviderGetListResponse, error) {

	var (
		resp   = &organization_service.ProviderGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			phone_number,
			active,
			created_at,
			updated_at		
		FROM provider
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Search != "" {
		where += ` AND name ILIKE '%' || '` + req.Search + `' || '%' AND phone_number ILIKE '%' || '` + req.Search + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id           sql.NullString
			name         sql.NullString
			phone_number sql.NullString
			active       sql.NullString
			created_at   sql.NullString
			updated_at   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&phone_number,
			&active,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.Providers = append(resp.Providers, &organization_service.Provider{
			Id:          id.String,
			Name:        name.String,
			PhoneNumber: phone_number.String,
			Active:      active.String,
			CreatedAt:   created_at.String,
			UpdatedAt:   updated_at.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *ProviderRepo) Update(ctx context.Context, req *organization_service.ProviderUpdate) (*organization_service.Provider, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			provider
		SET
			name = :name,
			phone_number = :phone_number,
			active =: active,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":           req.GetId(),
		"name":         req.GetName(),
		"phone_number": req.GetPhoneNumber(),
		"active":       req.GetActive(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &organization_service.Provider{
		Id:          req.Id,
		Name:        req.Name,
		PhoneNumber: req.PhoneNumber,
		Active:      req.Active,
	}, nil
}

func (r *ProviderRepo) Delete(ctx context.Context, req *organization_service.ProviderPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM provider WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
