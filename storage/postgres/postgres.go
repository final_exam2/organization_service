package postgres

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/final_exam2/organization_service/config"
	"gitlab.com/final_exam2/organization_service/storage"
)

type Store struct {
	db       *pgxpool.Pool
	branch   *BranchRepo
	provider *ProviderRepo
	market   *MarketRepo
	staff    *StaffRepo
}

func NewPostgres(ctx context.Context, cfg config.Config) (storage.StorageI, error) {
	config, err := pgxpool.ParseConfig(
		fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable",
			cfg.PostgresUser,
			cfg.PostgresPassword,
			cfg.PostgresHost,
			cfg.PostgresPort,
			cfg.PostgresDatabase,
		),
	)
	if err != nil {
		return nil, err
	}

	config.MaxConns = cfg.PostgresMaxConnections
	pool, err := pgxpool.ConnectConfig(ctx, config)
	if err != nil {
		return nil, err
	}

	return &Store{
		db: pool,
	}, nil

}

func (s *Store) CloseDB() {
	s.db.Close()
}

func (s *Store) Branch() storage.BranchRepoI {
	if s.branch == nil {
		s.branch = NewBranchRepo(s.db)
	}

	return s.branch
}

func (s *Store) Provider() storage.ProviderRepoI {
	if s.provider == nil {
		s.provider = NewProviderRepo(s.db)
	}

	return s.provider
}

func (s *Store) Market() storage.MarketRepoI {
	if s.market == nil {
		s.market = NewMarketRepo(s.db)
	}

	return s.market
}

func (s *Store) Staff() storage.StaffRepoI {
	if s.staff == nil {
		s.staff = NewStaffRepo(s.db)
	}

	return s.staff
}
