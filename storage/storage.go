package storage

import (
	"context"

	"gitlab.com/final_exam2/organization_service/genproto/organization_service"
)

type StorageI interface {
	CloseDB()
	Branch() BranchRepoI
	Provider() ProviderRepoI
	Market() MarketRepoI
	Staff() StaffRepoI
}

type BranchRepoI interface {
	Create(context.Context, *organization_service.BranchCreate) (*organization_service.Branch, error)
	GetByID(context.Context, *organization_service.BranchPrimaryKey) (*organization_service.Branch, error)
	GetList(context.Context, *organization_service.BranchGetListRequest) (*organization_service.BranchGetListResponse, error)
	Update(context.Context, *organization_service.BranchUpdate) (*organization_service.Branch, error)
	Delete(context.Context, *organization_service.BranchPrimaryKey) error
}

type ProviderRepoI interface {
	Create(context.Context, *organization_service.ProviderCreate) (*organization_service.Provider, error)
	GetByID(context.Context, *organization_service.ProviderPrimaryKey) (*organization_service.Provider, error)
	GetList(context.Context, *organization_service.ProviderGetListRequest) (*organization_service.ProviderGetListResponse, error)
	Update(context.Context, *organization_service.ProviderUpdate) (*organization_service.Provider, error)
	Delete(context.Context, *organization_service.ProviderPrimaryKey) error
}

type MarketRepoI interface {
	Create(context.Context, *organization_service.MarketCreate) (*organization_service.Market, error)
	GetByID(context.Context, *organization_service.MarketPrimaryKey) (*organization_service.Market, error)
	GetList(context.Context, *organization_service.MarketGetListRequest) (*organization_service.MarketGetListResponse, error)
	Update(context.Context, *organization_service.MarketUpdate) (*organization_service.Market, error)
	Delete(context.Context, *organization_service.MarketPrimaryKey) error
}

type StaffRepoI interface {
	Create(context.Context, *organization_service.StaffCreate) (*organization_service.Staff, error)
	GetByID(context.Context, *organization_service.StaffPrimaryKey) (*organization_service.Staff, error)
	GetList(context.Context, *organization_service.StaffGetListRequest) (*organization_service.StaffGetListResponse, error)
	Update(context.Context, *organization_service.StaffUpdate) (*organization_service.Staff, error)
	Delete(context.Context, *organization_service.StaffPrimaryKey) error
}
