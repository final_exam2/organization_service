// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.1
// 	protoc        v3.21.12
// source: sale.proto

package sale_service

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type Sale struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id        string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	BranchId  string `protobuf:"bytes,2,opt,name=branch_id,json=branchId,proto3" json:"branch_id,omitempty"`
	ShiftId   string `protobuf:"bytes,3,opt,name=shift_id,json=shiftId,proto3" json:"shift_id,omitempty"`
	MarketId  string `protobuf:"bytes,4,opt,name=market_id,json=marketId,proto3" json:"market_id,omitempty"`
	StaffId   string `protobuf:"bytes,5,opt,name=staff_id,json=staffId,proto3" json:"staff_id,omitempty"`
	Status    string `protobuf:"bytes,6,opt,name=status,proto3" json:"status,omitempty"`
	PaymentId string `protobuf:"bytes,7,opt,name=payment_id,json=paymentId,proto3" json:"payment_id,omitempty"`
	SaleId    string `protobuf:"bytes,8,opt,name=saleId,proto3" json:"saleId,omitempty"`
	CreatedAt string `protobuf:"bytes,9,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	UpdatedAt string `protobuf:"bytes,10,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at,omitempty"`
}

func (x *Sale) Reset() {
	*x = Sale{}
	if protoimpl.UnsafeEnabled {
		mi := &file_sale_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Sale) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Sale) ProtoMessage() {}

func (x *Sale) ProtoReflect() protoreflect.Message {
	mi := &file_sale_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Sale.ProtoReflect.Descriptor instead.
func (*Sale) Descriptor() ([]byte, []int) {
	return file_sale_proto_rawDescGZIP(), []int{0}
}

func (x *Sale) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *Sale) GetBranchId() string {
	if x != nil {
		return x.BranchId
	}
	return ""
}

func (x *Sale) GetShiftId() string {
	if x != nil {
		return x.ShiftId
	}
	return ""
}

func (x *Sale) GetMarketId() string {
	if x != nil {
		return x.MarketId
	}
	return ""
}

func (x *Sale) GetStaffId() string {
	if x != nil {
		return x.StaffId
	}
	return ""
}

func (x *Sale) GetStatus() string {
	if x != nil {
		return x.Status
	}
	return ""
}

func (x *Sale) GetPaymentId() string {
	if x != nil {
		return x.PaymentId
	}
	return ""
}

func (x *Sale) GetSaleId() string {
	if x != nil {
		return x.SaleId
	}
	return ""
}

func (x *Sale) GetCreatedAt() string {
	if x != nil {
		return x.CreatedAt
	}
	return ""
}

func (x *Sale) GetUpdatedAt() string {
	if x != nil {
		return x.UpdatedAt
	}
	return ""
}

type SalePrimaryKey struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *SalePrimaryKey) Reset() {
	*x = SalePrimaryKey{}
	if protoimpl.UnsafeEnabled {
		mi := &file_sale_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SalePrimaryKey) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SalePrimaryKey) ProtoMessage() {}

func (x *SalePrimaryKey) ProtoReflect() protoreflect.Message {
	mi := &file_sale_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SalePrimaryKey.ProtoReflect.Descriptor instead.
func (*SalePrimaryKey) Descriptor() ([]byte, []int) {
	return file_sale_proto_rawDescGZIP(), []int{1}
}

func (x *SalePrimaryKey) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

type SaleCreate struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	BranchId  string `protobuf:"bytes,1,opt,name=branch_id,json=branchId,proto3" json:"branch_id,omitempty"`
	ShiftId   string `protobuf:"bytes,2,opt,name=shift_id,json=shiftId,proto3" json:"shift_id,omitempty"`
	MarketId  string `protobuf:"bytes,3,opt,name=market_id,json=marketId,proto3" json:"market_id,omitempty"`
	StaffId   string `protobuf:"bytes,4,opt,name=staff_id,json=staffId,proto3" json:"staff_id,omitempty"`
	PaymentId string `protobuf:"bytes,5,opt,name=payment_id,json=paymentId,proto3" json:"payment_id,omitempty"`
}

func (x *SaleCreate) Reset() {
	*x = SaleCreate{}
	if protoimpl.UnsafeEnabled {
		mi := &file_sale_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SaleCreate) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SaleCreate) ProtoMessage() {}

func (x *SaleCreate) ProtoReflect() protoreflect.Message {
	mi := &file_sale_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SaleCreate.ProtoReflect.Descriptor instead.
func (*SaleCreate) Descriptor() ([]byte, []int) {
	return file_sale_proto_rawDescGZIP(), []int{2}
}

func (x *SaleCreate) GetBranchId() string {
	if x != nil {
		return x.BranchId
	}
	return ""
}

func (x *SaleCreate) GetShiftId() string {
	if x != nil {
		return x.ShiftId
	}
	return ""
}

func (x *SaleCreate) GetMarketId() string {
	if x != nil {
		return x.MarketId
	}
	return ""
}

func (x *SaleCreate) GetStaffId() string {
	if x != nil {
		return x.StaffId
	}
	return ""
}

func (x *SaleCreate) GetPaymentId() string {
	if x != nil {
		return x.PaymentId
	}
	return ""
}

type SaleUpdate struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id        string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	BranchId  string `protobuf:"bytes,2,opt,name=branch_id,json=branchId,proto3" json:"branch_id,omitempty"`
	ShiftId   string `protobuf:"bytes,3,opt,name=shift_id,json=shiftId,proto3" json:"shift_id,omitempty"`
	MarketId  string `protobuf:"bytes,4,opt,name=market_id,json=marketId,proto3" json:"market_id,omitempty"`
	StaffId   string `protobuf:"bytes,5,opt,name=staff_id,json=staffId,proto3" json:"staff_id,omitempty"`
	Status    string `protobuf:"bytes,6,opt,name=status,proto3" json:"status,omitempty"`
	PaymentId string `protobuf:"bytes,7,opt,name=payment_id,json=paymentId,proto3" json:"payment_id,omitempty"`
	SaleId    string `protobuf:"bytes,8,opt,name=saleId,proto3" json:"saleId,omitempty"`
}

func (x *SaleUpdate) Reset() {
	*x = SaleUpdate{}
	if protoimpl.UnsafeEnabled {
		mi := &file_sale_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SaleUpdate) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SaleUpdate) ProtoMessage() {}

func (x *SaleUpdate) ProtoReflect() protoreflect.Message {
	mi := &file_sale_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SaleUpdate.ProtoReflect.Descriptor instead.
func (*SaleUpdate) Descriptor() ([]byte, []int) {
	return file_sale_proto_rawDescGZIP(), []int{3}
}

func (x *SaleUpdate) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *SaleUpdate) GetBranchId() string {
	if x != nil {
		return x.BranchId
	}
	return ""
}

func (x *SaleUpdate) GetShiftId() string {
	if x != nil {
		return x.ShiftId
	}
	return ""
}

func (x *SaleUpdate) GetMarketId() string {
	if x != nil {
		return x.MarketId
	}
	return ""
}

func (x *SaleUpdate) GetStaffId() string {
	if x != nil {
		return x.StaffId
	}
	return ""
}

func (x *SaleUpdate) GetStatus() string {
	if x != nil {
		return x.Status
	}
	return ""
}

func (x *SaleUpdate) GetPaymentId() string {
	if x != nil {
		return x.PaymentId
	}
	return ""
}

func (x *SaleUpdate) GetSaleId() string {
	if x != nil {
		return x.SaleId
	}
	return ""
}

type SaleGetListRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Offset         int64  `protobuf:"varint,1,opt,name=offset,proto3" json:"offset,omitempty"`
	Limit          int64  `protobuf:"varint,2,opt,name=limit,proto3" json:"limit,omitempty"`
	SearchShiftId  string `protobuf:"bytes,3,opt,name=searchShiftId,proto3" json:"searchShiftId,omitempty"`
	SearchStaff    string `protobuf:"bytes,4,opt,name=searchStaff,proto3" json:"searchStaff,omitempty"`
	SearchBySaleId string `protobuf:"bytes,5,opt,name=searchBySaleId,proto3" json:"searchBySaleId,omitempty"`
	SearchBybranch string `protobuf:"bytes,6,opt,name=searchBybranch,proto3" json:"searchBybranch,omitempty"`
}

func (x *SaleGetListRequest) Reset() {
	*x = SaleGetListRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_sale_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SaleGetListRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SaleGetListRequest) ProtoMessage() {}

func (x *SaleGetListRequest) ProtoReflect() protoreflect.Message {
	mi := &file_sale_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SaleGetListRequest.ProtoReflect.Descriptor instead.
func (*SaleGetListRequest) Descriptor() ([]byte, []int) {
	return file_sale_proto_rawDescGZIP(), []int{4}
}

func (x *SaleGetListRequest) GetOffset() int64 {
	if x != nil {
		return x.Offset
	}
	return 0
}

func (x *SaleGetListRequest) GetLimit() int64 {
	if x != nil {
		return x.Limit
	}
	return 0
}

func (x *SaleGetListRequest) GetSearchShiftId() string {
	if x != nil {
		return x.SearchShiftId
	}
	return ""
}

func (x *SaleGetListRequest) GetSearchStaff() string {
	if x != nil {
		return x.SearchStaff
	}
	return ""
}

func (x *SaleGetListRequest) GetSearchBySaleId() string {
	if x != nil {
		return x.SearchBySaleId
	}
	return ""
}

func (x *SaleGetListRequest) GetSearchBybranch() string {
	if x != nil {
		return x.SearchBybranch
	}
	return ""
}

type SaleGetListResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Count int64   `protobuf:"varint,1,opt,name=count,proto3" json:"count,omitempty"`
	Sales []*Sale `protobuf:"bytes,2,rep,name=Sales,proto3" json:"Sales,omitempty"`
}

func (x *SaleGetListResponse) Reset() {
	*x = SaleGetListResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_sale_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SaleGetListResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SaleGetListResponse) ProtoMessage() {}

func (x *SaleGetListResponse) ProtoReflect() protoreflect.Message {
	mi := &file_sale_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SaleGetListResponse.ProtoReflect.Descriptor instead.
func (*SaleGetListResponse) Descriptor() ([]byte, []int) {
	return file_sale_proto_rawDescGZIP(), []int{5}
}

func (x *SaleGetListResponse) GetCount() int64 {
	if x != nil {
		return x.Count
	}
	return 0
}

func (x *SaleGetListResponse) GetSales() []*Sale {
	if x != nil {
		return x.Sales
	}
	return nil
}

var File_sale_proto protoreflect.FileDescriptor

var file_sale_proto_rawDesc = []byte{
	0x0a, 0x0a, 0x73, 0x61, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x0c, 0x73, 0x61,
	0x6c, 0x65, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x22, 0x93, 0x02, 0x0a, 0x04, 0x53,
	0x61, 0x6c, 0x65, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x02, 0x69, 0x64, 0x12, 0x1b, 0x0a, 0x09, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x5f, 0x69, 0x64,
	0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x49, 0x64,
	0x12, 0x19, 0x0a, 0x08, 0x73, 0x68, 0x69, 0x66, 0x74, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x07, 0x73, 0x68, 0x69, 0x66, 0x74, 0x49, 0x64, 0x12, 0x1b, 0x0a, 0x09, 0x6d,
	0x61, 0x72, 0x6b, 0x65, 0x74, 0x5f, 0x69, 0x64, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08,
	0x6d, 0x61, 0x72, 0x6b, 0x65, 0x74, 0x49, 0x64, 0x12, 0x19, 0x0a, 0x08, 0x73, 0x74, 0x61, 0x66,
	0x66, 0x5f, 0x69, 0x64, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x73, 0x74, 0x61, 0x66,
	0x66, 0x49, 0x64, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18, 0x06, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x12, 0x1d, 0x0a, 0x0a, 0x70,
	0x61, 0x79, 0x6d, 0x65, 0x6e, 0x74, 0x5f, 0x69, 0x64, 0x18, 0x07, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x09, 0x70, 0x61, 0x79, 0x6d, 0x65, 0x6e, 0x74, 0x49, 0x64, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x61,
	0x6c, 0x65, 0x49, 0x64, 0x18, 0x08, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x73, 0x61, 0x6c, 0x65,
	0x49, 0x64, 0x12, 0x1d, 0x0a, 0x0a, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74,
	0x18, 0x09, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x41,
	0x74, 0x12, 0x1d, 0x0a, 0x0a, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18,
	0x0a, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74,
	0x22, 0x20, 0x0a, 0x0e, 0x53, 0x61, 0x6c, 0x65, 0x50, 0x72, 0x69, 0x6d, 0x61, 0x72, 0x79, 0x4b,
	0x65, 0x79, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02,
	0x69, 0x64, 0x22, 0x9b, 0x01, 0x0a, 0x0a, 0x53, 0x61, 0x6c, 0x65, 0x43, 0x72, 0x65, 0x61, 0x74,
	0x65, 0x12, 0x1b, 0x0a, 0x09, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x5f, 0x69, 0x64, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x49, 0x64, 0x12, 0x19,
	0x0a, 0x08, 0x73, 0x68, 0x69, 0x66, 0x74, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x07, 0x73, 0x68, 0x69, 0x66, 0x74, 0x49, 0x64, 0x12, 0x1b, 0x0a, 0x09, 0x6d, 0x61, 0x72,
	0x6b, 0x65, 0x74, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x6d, 0x61,
	0x72, 0x6b, 0x65, 0x74, 0x49, 0x64, 0x12, 0x19, 0x0a, 0x08, 0x73, 0x74, 0x61, 0x66, 0x66, 0x5f,
	0x69, 0x64, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x73, 0x74, 0x61, 0x66, 0x66, 0x49,
	0x64, 0x12, 0x1d, 0x0a, 0x0a, 0x70, 0x61, 0x79, 0x6d, 0x65, 0x6e, 0x74, 0x5f, 0x69, 0x64, 0x18,
	0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x70, 0x61, 0x79, 0x6d, 0x65, 0x6e, 0x74, 0x49, 0x64,
	0x22, 0xdb, 0x01, 0x0a, 0x0a, 0x53, 0x61, 0x6c, 0x65, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x12,
	0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12,
	0x1b, 0x0a, 0x09, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x08, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x49, 0x64, 0x12, 0x19, 0x0a, 0x08,
	0x73, 0x68, 0x69, 0x66, 0x74, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07,
	0x73, 0x68, 0x69, 0x66, 0x74, 0x49, 0x64, 0x12, 0x1b, 0x0a, 0x09, 0x6d, 0x61, 0x72, 0x6b, 0x65,
	0x74, 0x5f, 0x69, 0x64, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x6d, 0x61, 0x72, 0x6b,
	0x65, 0x74, 0x49, 0x64, 0x12, 0x19, 0x0a, 0x08, 0x73, 0x74, 0x61, 0x66, 0x66, 0x5f, 0x69, 0x64,
	0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x73, 0x74, 0x61, 0x66, 0x66, 0x49, 0x64, 0x12,
	0x16, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x12, 0x1d, 0x0a, 0x0a, 0x70, 0x61, 0x79, 0x6d, 0x65,
	0x6e, 0x74, 0x5f, 0x69, 0x64, 0x18, 0x07, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x70, 0x61, 0x79,
	0x6d, 0x65, 0x6e, 0x74, 0x49, 0x64, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x61, 0x6c, 0x65, 0x49, 0x64,
	0x18, 0x08, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x73, 0x61, 0x6c, 0x65, 0x49, 0x64, 0x22, 0xda,
	0x01, 0x0a, 0x12, 0x53, 0x61, 0x6c, 0x65, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x16, 0x0a, 0x06, 0x6f, 0x66, 0x66, 0x73, 0x65, 0x74, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x06, 0x6f, 0x66, 0x66, 0x73, 0x65, 0x74, 0x12, 0x14, 0x0a,
	0x05, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x6c, 0x69,
	0x6d, 0x69, 0x74, 0x12, 0x24, 0x0a, 0x0d, 0x73, 0x65, 0x61, 0x72, 0x63, 0x68, 0x53, 0x68, 0x69,
	0x66, 0x74, 0x49, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0d, 0x73, 0x65, 0x61, 0x72,
	0x63, 0x68, 0x53, 0x68, 0x69, 0x66, 0x74, 0x49, 0x64, 0x12, 0x20, 0x0a, 0x0b, 0x73, 0x65, 0x61,
	0x72, 0x63, 0x68, 0x53, 0x74, 0x61, 0x66, 0x66, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b,
	0x73, 0x65, 0x61, 0x72, 0x63, 0x68, 0x53, 0x74, 0x61, 0x66, 0x66, 0x12, 0x26, 0x0a, 0x0e, 0x73,
	0x65, 0x61, 0x72, 0x63, 0x68, 0x42, 0x79, 0x53, 0x61, 0x6c, 0x65, 0x49, 0x64, 0x18, 0x05, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x0e, 0x73, 0x65, 0x61, 0x72, 0x63, 0x68, 0x42, 0x79, 0x53, 0x61, 0x6c,
	0x65, 0x49, 0x64, 0x12, 0x26, 0x0a, 0x0e, 0x73, 0x65, 0x61, 0x72, 0x63, 0x68, 0x42, 0x79, 0x62,
	0x72, 0x61, 0x6e, 0x63, 0x68, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0e, 0x73, 0x65, 0x61,
	0x72, 0x63, 0x68, 0x42, 0x79, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x22, 0x55, 0x0a, 0x13, 0x53,
	0x61, 0x6c, 0x65, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e,
	0x73, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28,
	0x03, 0x52, 0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x12, 0x28, 0x0a, 0x05, 0x53, 0x61, 0x6c, 0x65,
	0x73, 0x18, 0x02, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x12, 0x2e, 0x73, 0x61, 0x6c, 0x65, 0x5f, 0x73,
	0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x53, 0x61, 0x6c, 0x65, 0x52, 0x05, 0x53, 0x61, 0x6c,
	0x65, 0x73, 0x42, 0x17, 0x5a, 0x15, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x73,
	0x61, 0x6c, 0x65, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x62, 0x06, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x33,
}

var (
	file_sale_proto_rawDescOnce sync.Once
	file_sale_proto_rawDescData = file_sale_proto_rawDesc
)

func file_sale_proto_rawDescGZIP() []byte {
	file_sale_proto_rawDescOnce.Do(func() {
		file_sale_proto_rawDescData = protoimpl.X.CompressGZIP(file_sale_proto_rawDescData)
	})
	return file_sale_proto_rawDescData
}

var file_sale_proto_msgTypes = make([]protoimpl.MessageInfo, 6)
var file_sale_proto_goTypes = []interface{}{
	(*Sale)(nil),                // 0: sale_service.Sale
	(*SalePrimaryKey)(nil),      // 1: sale_service.SalePrimaryKey
	(*SaleCreate)(nil),          // 2: sale_service.SaleCreate
	(*SaleUpdate)(nil),          // 3: sale_service.SaleUpdate
	(*SaleGetListRequest)(nil),  // 4: sale_service.SaleGetListRequest
	(*SaleGetListResponse)(nil), // 5: sale_service.SaleGetListResponse
}
var file_sale_proto_depIdxs = []int32{
	0, // 0: sale_service.SaleGetListResponse.Sales:type_name -> sale_service.Sale
	1, // [1:1] is the sub-list for method output_type
	1, // [1:1] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_sale_proto_init() }
func file_sale_proto_init() {
	if File_sale_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_sale_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Sale); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_sale_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SalePrimaryKey); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_sale_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SaleCreate); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_sale_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SaleUpdate); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_sale_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SaleGetListRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_sale_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SaleGetListResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_sale_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   6,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_sale_proto_goTypes,
		DependencyIndexes: file_sale_proto_depIdxs,
		MessageInfos:      file_sale_proto_msgTypes,
	}.Build()
	File_sale_proto = out.File
	file_sale_proto_rawDesc = nil
	file_sale_proto_goTypes = nil
	file_sale_proto_depIdxs = nil
}
